cc.Class({
    extends: cc.Component,

    properties: {
        edbox:{
            default: null,
            type: cc.EditBox
        },

        btnSee:{
            default: null,
            type: cc.Button
        },
        
        _isSee:false,
        _string:""
    },

    // use this for initialization
    onLoad: function () {
    },

    onClear: function() {
        this.edbox.string = "";
    },

    onSee: function() {
      //  cc.log("onSee");
        this._isSee = !this._isSee;
        var str = this.edbox.string;
        if(this._isSee)
        {
     //       cc.log("See");
            this.edbox.inputFlag = 1;         
            this.btnSee.node.getChildByName("spUnSelected").active = false;
            this.btnSee.node.getChildByName("spSelected").active = true;
            this.edbox.string = str; 
        }
        else
        {
    //        cc.log("unSee");     
            this.edbox.inputFlag = 0;
            this.btnSee.node.getChildByName("spUnSelected").active = true;
            this.btnSee.node.getChildByName("spSelected").active = false;
            this.edbox.string = str;
        }
    },

     //当键盘弹出编辑框获得焦点时调用
    onEditDidBegan: function(editbox, customEventData) {
        editbox.placeholder = "";
    },

    //当键盘消失编辑框失去焦点时调用
    onEditDidEnded: function(editbox, customEventData) {
    },

    //当返回键按下时或者点击了键盘以外的区域时调用
    onEditingReturn: function(editbox,  customEventData) {
    },

    //当编辑框文本改变时调用
    onTextChanged: function(text, editbox, customEventData) {
        var txt= text;
    }

});
