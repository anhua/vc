/**
 * !#zh 快3走势图走势组件
 * @information 开奖号，和值，跨度。。
 */
cc.Class({
    extends: cc.Component,

    properties: {
         labIssue:{
            default:null,
            type:cc.Label
        },

        labNum1:{
            default:null,
            type:cc.Label
        },

        labNum2:{
            default:null,
            type:cc.Label
        },

        labNum3:{
            default:null,
            type:cc.Label
        },

        ndNum4:{
            default:null,
            type:cc.Node
        },

        spBgColor:{
            default:null,
            type:cc.Node
        },

        pfNumItem:{
            default:null,
            type:cc.Prefab
        },
        pfSpNumItem:{
            default:null,
            type:cc.Prefab
        },

        _data:null

    },

    // use this for initialization
    onLoad: function () {
        if(this._data != null)
        {
            this.spBgColor.color = this._data.color;
            var isus =  this._data.Isuse.toString();
            var isuseStr = isus.substring(isus.length-2,isus.length) + "期";
            this.labIssue.string = isuseStr;
            var numStr = "";
            var tempNum = -1;
            var openNums = -1;
            var countNum = 1;
            for(var i=0;i<this._data.num1.length;i++)
            {
                numStr += this._data.num1[i]+" ";
                if(this._data.num1[i] == tempNum)
                {
                    countNum++;
                    openNums = this._data.num1[i];
                }
                else
                {
                    tempNum = this._data.num1[i];
                }
            }
            this.labNum1.string = numStr;
            this.labNum2.string = this._data.num2;
            this.labNum3.string = this._data.num3;
            for(var i=0;i<this._data.num4.length;i++)
            {
                if(0 != this._data.num4[i])
                {
                    var numItem = cc.instantiate(this.pfNumItem);
                    var labNum = numItem.getChildByName("labNum");
                    labNum.getComponent(cc.Label).string = this._data.num4[i];
                    this.ndNum4.addChild(numItem);
                }
                else
                {                    
                    var spItem = cc.instantiate(this.pfSpNumItem);
                    var labNum = spItem.getChildByName("labNum");
                    labNum.getComponent(cc.Label).string = i+1;

                    if(openNums>-1 && openNums == i+1)
                    {
                        var small = spItem.getChildByName("spSamll");
                        small.active = true; 
                        small.getChildByName("labSmallNum").getComponent(cc.Label).string = countNum; 
                    }
                    this.ndNum4.addChild(spItem);
                }
            }
        }
    },

    /** 
    * 接收走势图走势信息
    * @method init
    * @param {Object} data
    */
    init:function(data){
        this._data = data;
    }

});
