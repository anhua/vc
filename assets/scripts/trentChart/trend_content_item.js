/**
 * !#zh 走势图走势组件
 * @information 走势
 */
cc.Class({
    extends: cc.Component,

    properties: {
        pfNumItem:{
            default:null,
            type:cc.Prefab
        },

        pfNumItem1:{
            default:null,
            type:cc.Prefab
        },

        pfRedItem:{
            default:null,
            type:cc.Prefab
        },

        pfBlueItem:{
            default:null,
            type:cc.Prefab
        },

        pfColorItem:{
            default:null,
            type:cc.Prefab
        },

        _data:null,
        _openNode:null,
        _itemId:null
    },

    // use this for initialization
    onLoad: function () {
        if(this._data != null)
        {
            this.node.color = this._data.color;
            if(this._data.isSpecial == 1)
            {
                if(this._data.type == "3")
                {
                    for(var i=0;i<this._data.rt.length;i++)
                    {
                        var numItem1 = cc.instantiate(this.pfNumItem1);
                        var labNum = numItem1.getChildByName("labNum");
                        labNum.getComponent(cc.Label).string = this._data.rt[i];
                        labNum.color = this._data.numColor;
                        this.node.addChild(numItem1);  
                    }
                }
                else
                {
                    for(var i=0;i<this._data.rt.length;i++)
                    {
                        var numItem = cc.instantiate(this.pfNumItem);
                        var labNum = numItem.getChildByName("labNum");
                        labNum.getComponent(cc.Label).string = this._data.rt[i];
                        labNum.color = this._data.numColor;
                        this.node.addChild(numItem);  

                    }
                }
            }
            else if(this._data.isSpecial == 0)
            {
                if(this._data.type == "1")//red
                {
                    for(var i=0;i<this._data.rt.length;i++)
                    {
                        if(0 != this._data.rt[i])
                        {
                            var numItem = cc.instantiate(this.pfNumItem);
                            var labNum = numItem.getChildByName("labNum");
                            labNum.getComponent(cc.Label).string = this._data.rt[i];
                            this.node.addChild(numItem);

                        }
                        else
                        {
                            var redItem = cc.instantiate(this.pfRedItem);
                            var labNum = redItem.getChildByName("labNum");
                            labNum.getComponent(cc.Label).string = this.splitTwo(i+1);
                            this.node.addChild(redItem);
                        }
                    }
                }
                else if(this._data.type == "2")//blue
                {
                    for(var i=0;i<this._data.rt.length;i++)
                    {
                        if(0 != this._data.rt[i])
                        {
                            var numItem = cc.instantiate(this.pfNumItem);
                            var labNum = numItem.getChildByName("labNum");
                            labNum.getComponent(cc.Label).string = this._data.rt[i];
                            this.node.addChild(numItem);  
                        }
                        else
                        {
                            var blueItem = cc.instantiate(this.pfBlueItem);
                            var labNum = blueItem.getChildByName("labNum");
                            labNum.getComponent(cc.Label).string = this.splitTwo(i+1);
                            this.node.addChild(blueItem);
                            this._openNode = blueItem;
                        }
                    }
                }
                else if(this._data.type == "3")//k3
                {
                    for(var i=0;i<this._data.rt.length;i++)
                    {
                        if(0 != this._data.rt[i])
                        {
                            var numItem = cc.instantiate(this.pfNumItem1);
                            var labNum = numItem.getChildByName("labNum");
                            labNum.getComponent(cc.Label).string = this._data.rt[i];
                            this.node.addChild(numItem);  
                        }
                        else
                        {
                            var colorItem = cc.instantiate(this.pfColorItem);
                            var labNum = colorItem.getChildByName("labNum");
                            labNum.getComponent(cc.Label).string = i+3;
                            this.node.addChild(colorItem);
                            this._openNode = colorItem;
                        }
                    }
                }
            } 
            
        }
    },

    /** 
    * 将数字转换成字符串且长度为2
    * @method splitTwo
    * @param {Number} num
    * @return {String} 长度为2的字符串
    */
    splitTwo:function(num){
        return num.toString().length<2?"0"+num.toString():num.toString();
    },

    /** 
    * 接收走势图走势信息
    * @method init
    * @param {Object} data
    */
    init:function(data){
        this._data = data;
    },

    /** 
    * 接收走势图期号列表id
    * @method onItemIndex
    * @param {Number} i
    */
    onItemIndex: function(i){
        this._itemId = i;
    },

    /** 
    * 刷新走势图走势信息
    * @method updateData
    * @param {Object} data
    */
    updateData: function (data) {
        this.node.removeAllChildren();
        if(data != null)
        {
            this.node.color = data.color;
            if(data.isSpecial == 1)
            {
                if(data.type == "3")
                {
                    for(var i=0;i<data.rt.length;i++)
                    {
                        var numItem1 = cc.instantiate(this.pfNumItem1);
                        var labNum = numItem1.getChildByName("labNum");
                        labNum.getComponent(cc.Label).string = data.rt[i];
                        labNum.color = data.numColor;
                        this.node.addChild(numItem1);  
 
                    }
                }
                else
                {
                    for(var i=0;i<data.rt.length;i++)
                    {
                        var numItem = cc.instantiate(this.pfNumItem);
                        var labNum = numItem.getChildByName("labNum");
                        labNum.getComponent(cc.Label).string =data.rt[i];
                        labNum.color = data.numColor;
                        this.node.addChild(numItem);  

                    }
                }
            }
            else if(data.isSpecial == 0)
            {
                if(data.type == "1")//red
                {
                    for(var i=0;i<data.rt.length;i++)
                    {
                        if(0 != data.rt[i])
                        {
                            var numItem = cc.instantiate(this.pfNumItem);
                            var labNum = numItem.getChildByName("labNum");
                            labNum.getComponent(cc.Label).string = data.rt[i];
                            this.node.addChild(numItem);
                            
                        }
                        else
                        {
                            var redItem = cc.instantiate(this.pfRedItem);
                            var labNum = redItem.getChildByName("labNum");
                            labNum.getComponent(cc.Label).string = this.splitTwo(i+1);
                            this.node.addChild(redItem);
                           
                        }
                    }
                }
                else if(data.type == "2")//blue
                {
                    for(var i=0;i<data.rt.length;i++)
                    {
                        if(0 != data.rt[i])
                        {
                            var numItem = cc.instantiate(this.pfNumItem);
                            var labNum = numItem.getChildByName("labNum");
                            labNum.getComponent(cc.Label).string = data.rt[i];
                            this.node.addChild(numItem);  
 
                        }
                        else
                        {
                            var blueItem = cc.instantiate(this.pfBlueItem);
                            var labNum = blueItem.getChildByName("labNum");
                            labNum.getComponent(cc.Label).string = this.splitTwo(i+1);
                            this.node.addChild(blueItem);
                            this._openNode = blueItem;

                        }
                    }
                }
                else if(data.type == "3")//k3
                {
                    for(var i=0;i<data.rt.length;i++)
                    {
                        if(0 != data.rt[i])
                        {
                            var numItem = cc.instantiate(this.pfNumItem1);
                            var labNum = numItem.getChildByName("labNum");
                            labNum.getComponent(cc.Label).string = data.rt[i];
                            this.node.addChild(numItem);  
                        }
                        else
                        {
                            var colorItem = cc.instantiate(this.pfColorItem);
                            var labNum = colorItem.getChildByName("labNum");
                            labNum.getComponent(cc.Label).string = i+3;
                            this.node.addChild(colorItem);
                            this._openNode = colorItem;
                        }
                    }
                }
            }  
        }
    }

});
