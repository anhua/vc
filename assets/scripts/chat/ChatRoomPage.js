/*
聊天室界面
*/ 
cc.Class({
    extends: cc.Component,

    properties: {
         //根目录
        content:{
            default:null,
            type:cc.Node
        },
        //聊天信息
        swChat:{
            default:null,
            type:cc.ScrollView
        },
        //投注面板
        betContent:{
            default:null,
            type:cc.Node
        },
        //输入信息条
        chatRoomItemIn:{
            default:null,
            type:cc.Prefab,
        },
        //输出信息条
        chatRoomItemOut:{
            default:null,
            type:cc.Prefab,
        },
        //输入投注信息条
        chatRoomItemBetIn:{
            default:null,
            type:cc.Prefab,
        },
        //输出投注信息条
        chatRoomItemBetOut:{
            default:null,
            type:cc.Prefab,
        },
        //系统消息
        chatRoomSystemInfo:{
            default:null,
            type:cc.Prefab,
        },

        //投注详情
        chatBetDetail:{
            default:null,
            type:cc.Prefab,
        },

        //聊天node
        chatEmotionContent:{
            default:null,
            type:cc.Node,
        },
        //表情栏
        emotionPanel:{
          default:null,
          type:cc.Node
        },
        //表情包
        emotionAtlas:{
            default:null,
            type:cc.SpriteAtlas
        },
        //表情页
        emotionContent:{
            default:null,
            type:cc.Node
        },
        //表情节点
        emotionNode:{
            default:null,
            type:cc.Node,
            name:""
        },
        //表情栏触摸范围外
        ndEmotionTouch:{
            default:null,
            type:cc.Node,
        },
        editBox:{
            default:null,
            type:cc.EditBox
        },

        betCollectionContent:{
            default:null,
            type:cc.Node
        },

        playRulesPagePrefab:{
            default:null,
            type:cc.Prefab
        },
        betResultPagePrefab:{
            default:null,
            type:cc.Prefab
        },

        //走势图列表 
        trentChartList:{
            default:[],
            type:cc.Prefab
        },

        //登录界面
        loginPopPrefab:{
            default:null,
            type:cc.Prefab
        },

        labChatName:{
            default:null,
            type:cc.Label
        },

        ndBet:{
            default:null,
            type:cc.Node
        },

        ndShowMoney:{
            default:null,
            type:cc.Node
        },

        rtMoney:{
            default:null,
            type:cc.RichText
        },

        verifiedPfb:cc.Prefab,//实名认证
        labNews: cc.Node,//推荐的资讯
        newsPagePrefab: cc.Prefab,   //新闻界面预制
        lotteryPagePrefab: cc.Prefab, //彩种分析列表界面预制
   
        _chatRoomPlayId:"",
        _roomName:"",
        _chatRoomID:"",
        _msgLists:[],
        _upTime:0,
        _reTime:0,

        _curMsgHeight:30,//当前聊天信息长度
        _curMsgOff:0,//当前偏移
        _isEmotionMoving:false,//表情展开中
        _isEmotionOpen:false,//是否打开表情
        _curShowTime:false,//当前显示时间
        _isAddMsg:true,//是否加入信息
        _reConect:false,//是否重连
        _isOut:false,//是否自己的消息
        _newsData:null,  //新闻资讯数据
        _isFristReJoin:true//是否首次加入房间 
    },

    onLoad: function () {
        this._reConect = false;
        this._isOut = false;
        if(User.getAppState() == 0)
        {
            this.ndBet.active = false;
        }
        this._msgLists = [];
        this._curMsgHeight = 30;
        this.labChatName.string = this._roomName;
        this.betContent.getComponent("BetControl").initBetControl(this._chatRoomPlayId);//彩票信息
        this.betCollectionContent.getComponent("BetCollection").init(this._chatRoomPlayId);  
        //右上角按钮
        var otherBtnContent = this.node.getChildByName("content").getChildByName("OtherBtnContent");
        otherBtnContent.getChildByName("bg").on(cc.Node.EventType.TOUCH_START, function (event) {
         var chatOtherBtnContent = otherBtnContent.getChildByName("ChatOtherBtnContent");
            var contentRect = chatOtherBtnContent.getBoundingBoxToWorld();
            var touchLocation = event.getLocation();
            if(cc.rectContainsPoint(contentRect, touchLocation) == false){
                otherBtnContent.active = false;
            }
        }.bind(this), this);

        this.ndEmotionTouch.on(cc.Node.EventType.TOUCH_START, function (event) {
            if(this.ndEmotionTouch.active == false)
                return;
            if(this.isEmotionOpening == true)
                return;
            if(this._isEmotionOpen == true)
            {
                this.emotionPanelEnd();
                return;
            }       
        }.bind(this), this);
        
        this.emotionContent.on(cc.Node.EventType.TOUCH_START, function (event) {
        }.bind(this), this);

        this.emotionContent.on(cc.Node.EventType.TOUCH_MOVE, function (event) {
        }, this);

        this.swChat.node.on(cc.Node.EventType.TOUCH_START, function (event) {
           
        }, this);
        //表情加载
        for(var i = 0; i<this.emotionAtlas.getSpriteFrames().length; i++){      
            var line_x = i%7;
            var line_y = parseInt(i/7)+1;   
            var emotionSpritFrame = this.emotionAtlas.getSpriteFrames()[i];
            var emotionNode = cc.instantiate(this.emotionNode);
            this.emotionContent.addChild(emotionNode);
            emotionNode.active = true;
            emotionNode.getComponent(cc.Sprite).spriteFrame = emotionSpritFrame;
            emotionNode.x = 150+ emotionNode.x + (line_x-1)*(128+20);
            emotionNode.y = emotionNode.y - (line_y-1)*(128+20);
            emotionNode.name = emotionSpritFrame.name;    
        }

        window.Notification.on("moneyShow",function(data){
            this.moneyShow();
        },this);

        window.Notification.on("gotoBet",function(data){
            this.betBtnCallback(null);
        },this);

        this.initEventHandlers();
        this.node.runAction(cc.sequence(cc.delayTime(1), cc.callFunc(function(){//延迟3s进行加载
            if(CL.NET.isConnect())
            {
                cc.log("chat joinroom");
                this.joinChatRoom(1);
            }
                
            this.swChat.node.active = false;
            this.node.runAction(cc.sequence(cc.delayTime(0.5), cc.callFunc(function(){//延迟3s进行加载
                this.swChat.node.active = true;
                this.swChat.node.emit("fade-in"); 
            }.bind(this))));
        }.bind(this))));

        if(this._chatRoomPlayId == "801" || this._chatRoomPlayId == "901" )
        {
            var recv = function(ret){
                if (ret.Code !== 0) {
                    cc.error(ret.Msg);
                    this.node.getChildByName('content').getChildByName('NewsPanelNode').active = false;
                }else{
                    if(ret.Data){
                        this.node.getChildByName('content').getChildByName('NewsPanelNode').active = true;
                        this._newsDataList =ret;
                        //-----
                        this._lotteryArr =[]; //存放是推荐的彩种分析列表
                        for(var i=0;i<ret.Data.length;++i){
                            if(ret.Data[i].IsRecommend){
                                this._lotteryArr.push(ret.Data[i]);
                            }
                        }

                        this.idx =0;
                        if(this._lotteryArr.length){
                            this.labNews.getComponent(cc.Label).string = this._lotteryArr[this.idx].Title;
                        }
                        this.schedule(this.scheLottery,2);  
                    }
                }
            }.bind(this);
            var data = {
                Token:User.getLoginToken(),
                LotteryCode: this._chatRoomPlayId,
            };
            CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.POSTNEWS, data, recv.bind(this),"POST");  
            //推荐的新闻资讯
            this.labNews.on('touchstart',this.onNews,this);
        }
    },

    /** 
    * 新闻资讯间隔几秒播放
    * @method scheLottery
    * @param {} 
    */
    scheLottery:function(){
        if(this._lotteryArr.length <= 0)
        {
            this.unschedule(this.scheLottery);
            return;
        }
            
        this.idx++;
        if(this.idx >this._lotteryArr.length-1){
            this.idx =0;
        }
        this.labNews.getComponent(cc.Label).string = this._lotteryArr[this.idx].Title;
        this._newsData =this._lotteryArr[this.idx];
    },

    /** 
    * 更新用户余额
    * @method updataMoney
    * @param {} 
    */
    moneyShow:function(){
        var recv = function(ret){
            if(ret.Code === 0)
            {
               
                User.setBalance(ret.Balance);
                User.setGoldBean(ret.Gold);
                this.rtMoney.string = "<color=#000000>您的投注已成功，当前账户余额：</color>" + "<color=#fffea9>" + User.getBalance().toFixed(2) + "</color>" + "<color=#000000> 元</color>";
                var callfun = cc.callFunc(function(){
                    this.ndShowMoney.opacity = 255;
                    this.ndShowMoney.active = false;
                }.bind(this));
                this.ndShowMoney.active = true;
                this.ndShowMoney.runAction(cc.sequence(cc.fadeOut(5), callfun));
            }
            else
            {
                ComponentsUtils.showTips(ret.Msg);
            }
        }.bind(this);
        var data = {
            Token:User.getLoginToken(),
            UserCode:User.getUserCode(),
        }
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.GETUSERMONEY, data, recv.bind(this),"POST");  
    },
    
    initChatRoom:function(strPlayId, strRoomId, strRoomName,chatRoomID){
        this._chatRoomPlayId = strRoomId;//彩种ID
        this._roomName = strRoomName;
        this._chatRoomID = chatRoomID;
    },

    //删除表情
    onEmotionDelBtn:function(){
        if(this.editBox.string != "")
        {
           var str1 = this.editBox.string.charAt(this.editBox.string.length -1);
           if(str1 == "]")
           {
               var str4 = this.editBox.string.substring(this.editBox.string.length -4);
               var index = str4.indexOf("["); 
               if(index != -1)
               {
                   var dellen = 4-index;
                   this.editBox.string = this.editBox.string.substring(0,this.editBox.string.length-dellen);
               }
           }
        }
    },

    update: function(dt){
        this._upTime += dt;
        this._reTime += dt;
        if(this._upTime > 0.1){  
            if(this._isAddMsg)
            {
                this._isAddMsg = false;
                if(this._msgLists.length>0)
                {
                    this.publicMsg( this._msgLists.splice(this._msgLists.length[0],1));
                    
                }
                this._isAddMsg = true;
            }
            this._upTime = 0;//每0.1秒重置为0，以达到循环显示  
        }  

        if(this._reTime > 2){  
            if(CL.NET.isConnect())
            {
                return;
            }
            if(this.betContent.getComponent("BetControl").getConnetState())
            {
                // cc.log("chatroom connecting");
                this._reConect = this._isFristReJoin?false:true;
                CL.MANAGECENTER.onConnectSK();
            }
            this._reTime = 0;//每2秒重置为0
        }  
    },

    addTimeMsg:function(time){
        var timeTxt = Utils.getYMDHMbyTime(time,"-",":");
        var rtcontent =  "<color=#ffffff>" + timeTxt + "</color>";
        var data0 = {
            rtContent:rtcontent,
            type:cc.TextAlignment.CENTER,
        };
        this.addSystemMsg(data0);
        this._curShowTime = time;
    },

    addShowTime :function(time,msgtype){
        if(msgtype == 1)//普通聊天信息
        {
            if(Math.abs(this._curShowTime-time)>=300)//间隔大于5分钟显示时间
            {
                 this.addTimeMsg(time);
            }
        }
        else if(msgtype == 2)//投注信息
        {
            if(Math.abs(this._curShowTime-time)>=60)//间隔大于1分钟显示时间
            {
                 this.addTimeMsg(time);
            }
        } 
    },

    //聊天信息重置聊天位置
    updateHeight:function(itemHeight,space,isout){
        var height = this.swChat.content.height;
        this._curMsgHeight += itemHeight+space;
     
        if(this._curMsgHeight >= 1400)
        {
            this.swChat.content.height = this._curMsgHeight;
            var offy = this.swChat.getScrollOffset().y; 
            if( this._curMsgOff != 0 &&( offy <= this._curMsgOff-200 || offy >= this._curMsgOff+200)) 
            {
                if(isout)
                    this.swChat.scrollToBottom(0); 
                return;
            }
            else
            {
                this.swChat.scrollToBottom(0); 
                this._curMsgOff = this.swChat.getScrollOffset().y; 
            } 
        }
    },

    publicMsg:function(msg){
        this._isOut = false;
        if( this.swChat.content.childrenCount>50)
        {
            this.swChat.content.removeChild(this.swChat.content.children[0]);
            this.updateHeight(-(this.swChat.content.children[0].height),-20,this._isOut);
        }
        
        var obj = msg[0].data;
        var curTime = msg[0].time;
        if(obj.MsgType == 2){//玩家聊天消息
            this.addShowTime(curTime,1);
            
            var item = null;
            var flow = "";
            if(obj.userid != User.getUserCode()){
                flow = "in";
                item = cc.instantiate(this.chatRoomItemIn);
            }else{
                flow = "out";
                this._isOut = true;
                item = cc.instantiate(this.chatRoomItemOut);
            }
            var data0 = {
                head:obj.avatarUrl,
                name:obj.nickName,
                txt:obj.content,
                flow:flow,
            };
           
            item.getComponent('chat_msg_outin_Item').init(data0); 
            this.swChat.content.addChild(item);
            this.updateHeight(item.height,20,this._isOut);
        }else if(obj.MsgType == 1){//系统消息
            var sysObj = obj;
            switch (sysObj.CustomMsgType )
            {
                case DEFINE.RECV_CHAT_MSG_TYPE.CHAT_PUBLIC_MuteList:
                {
                    ComponentsUtils.showTips("xxx被禁言");
                    var rtcontent = "<color=#00F7E8>" + sysObj.Name + "</color>" + "<color=#ffffff>" + sysObj.Content + "</color>";
                    var data0 = {
                        rtContent:rtcontent,
                        type:cc.TextAlignment.CENTER,
                    };
                    this.addSystemMsg(data0);
                }
                break;
                case DEFINE.RECV_CHAT_MSG_TYPE.CHAT_PUBLIC_Bet://投注消息
                {
                    this.addShowTime(curTime,2);
                   
                    var item = null;
                    if(sysObj.Data.Name == User.getNickName())
                    {
                        item = cc.instantiate(this.chatRoomItemBetOut);
                        this._isOut = true;
                    }
                    else
                    {
                        item = cc.instantiate(this.chatRoomItemBetIn);
                    }

                    var data0 = {
                        name:sysObj.Data.Name,
                        money:LotteryUtils.moneytoServer(sysObj.Amount),
                        isuse:sysObj.IsuseName,
                        lotteryName:sysObj.LotteryName,
                        buttype:sysObj.BuyType,
                        head:sysObj.Data.AvatarUrl,
                    };
                    item.getComponent('chat_msg_bet_outin_Item').init(data0); 

                    var detailData = {
                        data:sysObj,
                        IsuseName:sysObj.IsuseName,
                        Amount:LotteryUtils.moneytoServer(sysObj.Amount),
                        OrderCode:sysObj.OrderCode,
                        BuyType:sysObj.BuyType
                    };
                    var clickEventHandler = new cc.Component.EventHandler();
                    clickEventHandler.target = this.node; 
                    clickEventHandler.component = "ChatRoomPage"
                    clickEventHandler.handler = "onCheckBetCallBack";
                    clickEventHandler.customEventData = detailData;
                    item.getChildByName("spBg").getComponent(cc.Button).clickEvents.push(clickEventHandler);
                    this.swChat.content.addChild(item);    
                    this.updateHeight(item.height,20,this._isOut);
                }
                break;
                case DEFINE.RECV_CHAT_MSG_TYPE.CHAT_PUBLIC_Notice://系统公告
                {
                    var rtcontent = "<color=#ffffff>" + sysObj.Content + "</color>";
                    var data0 = {
                        rtContent:rtcontent,
                        type:cc.TextAlignment.CENTER,
                    };
                   
                    this.addSystemMsg(data0);
                }
                break;
                case DEFINE.RECV_CHAT_MSG_TYPE.CHAT_PUBLIC_Award://中奖消息
                {
                    var data0 = {
                        rtContent:sysObj.Content,
                        type:cc.TextAlignment.CENTER,
                    };
                   
                    this.addSystemMsg(data0);
                }
                break;
            } 
        }
        else if(obj.MsgType == 3)//玩家加入
        {
            this.addSystemMsg(obj);
        }
    },

    initEventHandlers:function(){
        var self = this;
        window.Notification.on('rjoin_push',function(data){
            CL.NET.roomid = self._chatRoomID;
        }.bind(this));

        window.Notification.on('otherjoin_push',function(data){//有玩家加入
            var obj  = data;
            if(cc.sys.isNative)
            {
                if(Object.prototype.toString.call(data) !== "object")
                {
                    obj  =  eval('(' + data + ')');
                }
            }
            obj.MsgType = 3;
            var obj0 = new Object();
            obj0.time = 0;
            obj0.data = obj;
            this._msgLists.push(obj0);
         
        }.bind(this));

        window.Notification.on('sysmsg_push',function(data){
            this._msgLists.push(data);
          
        }.bind(this));
        
        window.Notification.on('newmsg_push',function(data){
            this._msgLists.push(data);
          
        }.bind(this));

        window.Notification.on('rlogin_push',function(data){
            if(data)
            {
                this._isFristReJoin = false;
                this.joinChatRoom(0);
            }
            else
            {
                ComponentsUtils.showTips("登录失败!");
            }
        }.bind(this));
    },

    onCheckBetCallBack:function(event, customEventData){
        var canvas = cc.find("Canvas");
        var chatBetDetailPop = cc.instantiate(this.chatBetDetail);
        chatBetDetailPop.getComponent('chat_bet_detail_pop').init(customEventData); 
        canvas.addChild(chatBetDetailPop);
    },

    addSystemMsg:function(data)
    {
        //cc.TextAlignment.LEFT
        //cc.TextAlignment.CENTER
        //cc.TextAlignment.RIGHT
        var item = null;
        item = cc.instantiate(this.chatRoomSystemInfo);
  
        item.getComponent('chat_msg_system_Item').init(data); 
        try {
            this.swChat.content.addChild(item);
            this.updateHeight(item.height,20,false);
        } catch (error) {
            // cc.log("chat addSystemMsg Erro:",error);
        }
    },

    //新闻投注
    betByNewsCallBack:function(selNums){
        this.betBtnCallback();
        this.betCollectionContent.getComponent("BetCollection").betNumsByNew(selNums); 
    },

    //投注
    betBtnCallback:function(event){    
        if(this.isEmotionOpening == true)
            return;
        if(this._isEmotionOpen == true)
        {
            this.emotionPanelEnd();
            return;
        }

        if(User.getIsvermify())
        {
            this.betCollectionContent.getComponent("BetCollection").onOpenInfo(this._chatRoomID);   
        }
        else //前往登录
        {
            var callback = function callback(ret){
                if(ret && ret == true)
                {
                    this.betCollectionContent.getComponent("BetCollection").onOpenInfo(this._chatRoomID); 
                }   
            }.bind(this)
            this.goToLogin(callback);
        }   
    },

    joinChatRoom:function(isshow){
        //isshow 1是需要历史请求，0不需要历史请求
        var rtcontent = "<color=#00F7E8>" + User.getNickName() + "</color>" + "<color=#ffffff>"+ "进入了房间" + "</color>";
        var data = {
            rtContent:rtcontent,
            type:cc.TextAlignment.CENTER,
        };
    
        //this._reConect = false;
        var json = JSON.stringify(data); 
        cc.log("chat joinroom"+this._chatRoomID);
        CL.NET.joinRoom(this._chatRoomID,json,isshow);
    },

    //发送聊天信息
    onSendMsgBtn:function(event){
        if(!User.getIsvermify())
        {
            var callback = function callback(ret){
                if(ret && ret == true)
                {
                    this.onSendMsgBtn();
                }   
            }.bind(this)
            this.goToLogin(callback);
            return;
        }

        if(!User.getIsCertification())
        {
            var canvas = cc.find("Canvas");
            var verifiedPrefab = cc.instantiate(this.verifiedPfb);
            canvas.addChild(verifiedPrefab);
            return;
        }

        if(!CL.NET.isConnect())
        {
            // cc.log("chat room 房间内断连重连 ");
            return;
        }

        var editBoxText = this.editBox.string;
        editBoxText = editBoxText.replace(/^\s+|\s+$/g, '');

        if(editBoxText == ""){
            ComponentsUtils.showTips("不能发送空白信息");
        }else{ 
            var content = new Object(); 
            content.content = editBoxText;
            content.userid = User.getUserCode();
            content.MsgType = 2;
            content.avatarUrl = User.getAvataraddress();
            content.nickName = User.getNickName();
            var json = JSON.stringify(content); 
            this.editBox.string = "";
            CL.NET.send_group_msg(json);  
        }   
        
        if(this._isEmotionOpen == true)
        {
            this.emotionPanelEnd();
        } 
    },

    //打开表情栏
    emotionPanelBtnCallBack:function(event){
        if(this.isEmotionOpening == true)
            return;
        if(this._isEmotionOpen == true)
        {
            this.emotionPanelEnd();
            return;
        }
        if(this._isEmotionMoving == false)
        {
            this.ndEmotionTouch.active = true;
            this.chatEmotionContent.runAction(cc.sequence(cc.moveTo(0.2, cc.p(0,this.chatEmotionContent.y+440)), cc.callFunc(function(){
                this._isEmotionMoving = false;
                this._isEmotionOpen = true;
            }.bind(this))));  
            this._isEmotionMoving = true;
        }
    },

    //关闭表情栏
    emotionPanelEnd:function(event){
        if(this._isEmotionMoving == false)
        {
            this.ndEmotionTouch.active = false;
            this.chatEmotionContent.runAction(cc.sequence(cc.moveTo(0.2, cc.p(0,this.chatEmotionContent.y-440)), cc.callFunc(function(){
                this._isEmotionMoving = false;
                this._isEmotionOpen = false;
            }.bind(this))));  
            this._isEmotionMoving = true;
        }
    },

    //当键盘弹出编辑框获得焦点时调用
    onEditDidBegan: function(editbox, customEventData) {
        if(this.isEmotionOpening == true)
            return;
        if(this._isEmotionOpen == true)
        {
            this.emotionPanelEnd();
            return;
        }
    },

    //选择表情
    emotionBtnCallback:function(event){
        var currentTarget = event.getCurrentTarget();
        var emotionStr = "["+currentTarget.name+"]";
        if(this.editBox.string.length+ emotionStr.length>128)
        {
            return;
        }
        this.editBox.string = this.editBox.string + emotionStr;
    },

    //其他按钮
    chatOtherBtnCallback:function(event){
        var otherBtnContent = this.node.getChildByName("content").getChildByName("OtherBtnContent");
        if(otherBtnContent.active == true){
            otherBtnContent.active = false;
        }else{
            otherBtnContent.active = true;
        }  
    },
    
    //关闭界面
    backBtnCallback:function(event){
        CL.NET.leaveRoom(this._chatRoomID);

        window.Notification.offType("BET_ONPAY");
        window.Notification.offType("BET_ONCLOSE");
        window.Notification.offType("BET_NEXTPAGE");
        
        window.Notification.offType("newmsg_push");
        window.Notification.offType("sysmsg_push");
        window.Notification.offType("rjoin_push");
        window.Notification.offType("otherjoin_push");
        window.Notification.offType("rlogin_push");
        
        window.Notification.offType("gotoBet");
        window.Notification.offType("moneyShow");

        this.unschedule(this.scheLottery);

        this._msgLists = [];

        this.node.getComponent("Page").backAndRemove();
    },
    
    onClose:function(){
         this.backBtnCallback();
    },

    //规则
    playRuleBtnCallback:function(){
        var canvas = cc.find("Canvas");
        var playRulesPage = cc.instantiate(this.playRulesPagePrefab);
        playRulesPage.getComponent("PlayRulesPage").init(this._chatRoomPlayId);
        canvas.addChild(playRulesPage);
        var otherBtnContent = this.content.getChildByName("OtherBtnContent");
        otherBtnContent.active = false;
    },

    //开奖信息
    betInfoBtnCallback:function(){
        var canvas = cc.find("Canvas");
        var betResultPage = cc.instantiate(this.betResultPagePrefab);
        betResultPage.getComponent("BetResultPage").init(this._chatRoomPlayId);
        canvas.addChild(betResultPage);
        var otherBtnContent = this.content.getChildByName("OtherBtnContent");
        otherBtnContent.active = false;
    },

    //走势图
    onTrendChartCallback:function(){
        var trentchart = this.trentChartList[LotteryUtils.getPageByLotteryid(this._chatRoomPlayId)];
        if(trentchart != null && trentchart != undefined)
        {
            var canvas = cc.find("Canvas");
            var trentchartPage = cc.instantiate(trentchart);
            var data = {
                Lotteryid:this._chatRoomPlayId,
                playCode:this._chatRoomPlayId+"01",
            };
            trentchartPage.getComponent(trentchartPage.name).init(data,this);
            canvas.addChild(trentchartPage);
            var otherBtnContent = this.content.getChildByName("OtherBtnContent");
            otherBtnContent.active = false;
            this.node.active =false;
        }
    },
    
    //前往登录
    goToLogin:function(callback){
        var canvas = cc.find("Canvas");
        var loginPop = cc.instantiate(this.loginPopPrefab);
        canvas.addChild(loginPop);
        loginPop.getComponent("Login_login").setCallBack(callback.bind(this));
    },

    //新闻资讯
    onNews: function(){
        if(this._newsData)
        {
            var canvas = cc.find("Canvas");
            var news =cc.instantiate(this.newsPagePrefab);
            canvas.addChild(news); 
            news.getComponent('newsPage').init(this,this._newsData,false);
        }
    },

    //资讯彩种分析列表界面
    onLotteryList: function(){
        var canvas = cc.find("Canvas");
        var lotterys =cc.instantiate(this.lotteryPagePrefab);
        canvas.addChild(lotterys); 
        lotterys.getComponent('lotteryPage').init(this,this._newsDataList);
    }

});
