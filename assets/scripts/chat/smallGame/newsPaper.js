/*
新闻资讯标题组件
*/ 
cc.Class({
    extends: cc.Component,

    properties: {
        labStr: cc.RichText,
        newsPagePrefab: cc.Prefab,   //新闻界面预制
        labTime: cc.Label,   //日期
        _homeType: null
    },

    // use this for initialization
    onLoad: function () {
        this.node.on('touchstart',this.openPage,this)
    },

    init: function(data,data1,that,tData,tHome){
        //此列是否为推荐
        if(tData.Data.IsRecommend){
            this.labStr.string = '<color=#ff0000>'+'[荐] '+'</color>'+ data;
        }else{
            this.labStr.string = data;    
        }
        this._homeType =that;
        this._tData =tData;
        this._tHome =tHome;

        var date =Utils.formateServiceDateStrToDate(data1);
        this.labTime.string = this.splitTwo(date.getMonth() + 1)+'/'+ this.splitTwo(date.getDate());
    },

    /** 
    * 将数字转换成字符串且长度为2
    * @method splitTwo
    * @param {Number} num
    * @return {String} 长度为2的字符串
    */
    splitTwo:function(num){
        return num.toString().length<2?"0"+num.toString():num.toString();
    },

    //打开对应的新闻界面
    openPage: function(){
        var news =cc.instantiate(this.newsPagePrefab);
        this._homeType.node.addChild(news);   
        news.getComponent('newsPage').init(this._homeType,this._tData,true);
        this._tHome.getComponent("Page").backAndRemove();
    }

});
