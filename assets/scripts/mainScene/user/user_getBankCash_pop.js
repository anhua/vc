/**
 * 奖金提现界面
 */
cc.Class({
    extends: cc.Component,

    properties: {
        edMoney:{
            default: null,
            type: cc.EditBox
        },

        labBalance:{
            default: null,
            type: cc.Label
        },
        
        labCont:{
            default: null,
            type: cc.Label
        },

        //提现密码输入
        ndInputPwd: {
            default: null,
            type: cc.Node
        },

        //存在银行卡
        btnExistBankCard:{
            default: null,
            type: cc.Button
        },

        //添加银行卡
        btnNoBankCard:{
            default: null,
            type: cc.Button
        },

        //我的银行卡
        bankCardPop: {
            default: null,
            type: cc.Prefab
        },

        labBankType:{
            default: null,
            type: cc.Label
        },

        labBankLastNum:{
            default: null,
            type: cc.Label
        },

        ebPwd:{
            default:null,
            type:cc.EditBox,
            readonly:false
        },

        labDec1:{
            default: null,
            type: cc.Label
        },

        labDec2:{
            default: null,
            type: cc.Label
        },

        ndLists:{
            default:[],
            type:cc.Node
        },

        _bankCode:"",//银行卡编号
        _code:"",//输入的密码
        _isHasCard:false,//是否有银行卡

        _minMoney:10.00,//至少提现多少金额
        _maxCount:0,//最多提现多少次
        _withdrawMoney:0,//当前可提现金额
    },

    // use this for initialization
    onLoad: function () {
        this.initInfo();
        this.getOneCard();
        this.ndInputPwd.on(cc.Node.EventType.TOUCH_END, function (event) {
            if(!this.ndInputPwd.active)
                return;
            console.log('user_getbankcash_pop node  TOUCH_END');
        }, this);
        this.cash = "";
        this.ebPwd.string = "";
    },

    initInfo:function(){
        this.labCont.string = "0次";
         var recv = function(ret){
            ComponentsUtils.unblock();
            if(ret.Code != 0)
            {
                ComponentsUtils.showTips(ret.Msg);
                console.log(ret.Msg);
            }
            else
            {
                this._minMoney = 10.00;
                this._maxCount = ret.Count;
                //this.labDec1.string = "1.提现金额至少"+ this._minMoney +"元，只可提现中奖奖金";
                //this.labDec2.string = "2.每个账户每天最多提现"+ret.Count + "次";
                this._withdrawMoney = LotteryUtils.moneytoServer(ret.WithdrawMoney);
                this.labBalance.string = this._withdrawMoney +"元";
                this.labCont.string = ret.Count + "次";
                if(User.getBalance()>this._minMoney)
                {
                    this.edMoney.placeholder = "请输入提现金额";
                }
                else
                {
                    this.edMoney.placeholder = "可提现余额不足，无法提现";
                }
            }
        }.bind(this);          

        var data = {
            Token:User.getLoginToken(),
            UserCode:User.getUserCode(),
            b:false
        }
        CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.GETCASHINFO, data, recv.bind(this),"POST");  
        ComponentsUtils.block();
    },

    onTextChangedCash:function(text, editbox, customEventData){
        var txt = text;
        if( txt == "" )
        {
            this.cash = txt;
        } 
        else
        {
            this.cash = txt.replace(/[^\d.]/g,"");
            txt = this.cash;
            editbox.string = txt;
            return;
        }
     },

    onTextChanged: function(text, editbox, customEventData) {
        var txt = text;
        if(Utils.isInt(txt) || txt == "")
        {
            this._code = txt;
            for(var i=0;i<6;i++)
            {
                if(this._code.length>=i)
                    this.ndLists[i].active = true;  
                else
                    this.ndLists[i].active = false;
            }
        }
        else
        {
            txt = this._code;
            editbox.string = txt;
            return;
        }

        for(var i=0;i<6;i++)
        {
            if(this._code.length-1>=i)
                this.ndLists[i].active = true;  
            else
                this.ndLists[i].active = false;
        }

        if(this._code.length == 6)
        { 
            var recv = function(ret){
                ComponentsUtils.unblock();
                if(ret.Code !== 0)
                {
                    ComponentsUtils.showTips(ret.Msg);
                    console.log(ret.Msg);
                    this.onBack();
                }
                else
                {
                    window.Notification.emit("USER_useruirefresh","");
                    ComponentsUtils.showTips("提现申请成功！");
                    this.onClose();
                }
            }.bind(this);  

            var amont = LotteryUtils.moneytoClient(parseFloat(this.edMoney.string))
            var data = {
                Token:User.getLoginToken(),
                UserCode:User.getUserCode(),
                BankCode:this._bankCode,
                Amount:amont,
                PayPwd:parseInt(this._code)
            }
            CL.HTTP.sendRequest(DEFINE.HTTP_MESSAGE.WITHDRAWALS, data, recv.bind(this),"POST");  
            ComponentsUtils.block();
        }

    },


    //前往银行卡列表
    onGotoBankCard: function(){
        var canvas = cc.find("Canvas");
        var bankCard = cc.instantiate(this.bankCardPop);
        var Callback = function(ret){
      //      cc.log("选择银行卡回调")
            if(ret == null)
            {
                this.getOneCard();
            }
            else
            {
                this.btnExistBankCard.node.active = true;
                this.btnNoBankCard.node.active = false;
                this.labBankType.string = ret.bankname;
                this.labBankLastNum.string = ret.last;
                this._bankCode = ret.bankcode;
                this._isHasCard = true;
            }
        }.bind(this);
        bankCard.getComponent("user_bankCard_pop").init(1,Callback);
        canvas.addChild(bankCard);
       // bankCard.active = true;
    },

    //获取一张银行卡
    getOneCard:function(){
        var recv = function(ret){
            ComponentsUtils.unblock();
            if(ret.Code != 0 && ret.Code != 49)
            {
                ComponentsUtils.showTips(ret.Msg);
                console.log(ret.Msg);
                this.btnNoBankCard.node.active = true;
                this._isHasCard = false;
            }
            else
            {
                if(ret.Data != null)
                {
                    this.btnExistBankCard.node.active = true;
                    this.btnNoBankCard.node.active = false;
                    this.labBankType.string = ret.Data[0].BankName;
                    this.labBankLastNum.string = ret.Data[0].CardNum.substr(ret.Data[0].CardNum.length-4);
                    this._bankCode = ret.Data[0].BankCode;
                    this._isHasCard = true;
                }
                else
                {
                    this.btnNoBankCard.node.active = true;
                    this._isHasCard = false;
                }
            }
        }.bind(this);          

        var data = {
            Token:User.getLoginToken(),
            UserCode:User.getUserCode(),
            PageNumber:1,
            RowsPerPage:1,
        }
        CL.HTTP.sendRequestRet(DEFINE.HTTP_MESSAGE.GETBANKCARDLIST, data, recv.bind(this),"POST");   
        ComponentsUtils.block();
    },

    checkInput: function(){
        if(this.edMoney.string != "")
        {
            if(!Utils.isInt(this.edMoney.string) && !Utils.isFolatMoney(this.edMoney.string))
            {
                ComponentsUtils.showTips("输入金额错误");
                return false;
            }
        }
        else
        {
            ComponentsUtils.showTips("输入金额不能为空");
            return false;
        }
        return true;
    },

    onTrue: function(){
    
        if(!this._isHasCard)
        {
            ComponentsUtils.showTips("请先绑定银行卡");
            return;
        }
        if(this._maxCount<=0)
        {
            ComponentsUtils.showTips("当日提现次数已用完，请明日再试");
            return;
        }
        var money = this.edMoney.string;
        // if(Utils.isFolatMoney(money))
        // {
        //     ComponentsUtils.showTips("输入金额错误");
        //     return;
        // }

       
        if(this.checkInput())
        {
            var iMoney = parseFloat(money);
            if(this._withdrawMoney<iMoney)
            {
                ComponentsUtils.showTips("输入金额不能大于可提现金额");
                return;
            }
            if(iMoney<this._minMoney)
            {
                ComponentsUtils.showTips("输入金额不能小于"+this._minMoney);
                return;
            }
            // if(iMoney <= User.getBalance())
            // {
            //     ComponentsUtils.showTips("输入金额不能大于当前余额");
            //     return;
            // }

            this.ndInputPwd.active = true;
            this.ebPwd.setFocus();
        }
    },

    onBack: function(){
        this.ndInputPwd.active = false;
        this.ebPwd.string = "";
        this._code = "";
        for(var i=0;i<this.ndLists.length;i++)
        {
            this.ndLists[i].active = false;
        }
    },

    onClose: function(){
        this.onBack();
        this.node.getComponent("Page").backAndRemove();
    },

});
